
def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, repeat = 2)
  result = ""
  repeat.times {result << str + " "}
  result.split(" ").join(" ")
end

def start_of_word(word, char_num = 1)
  word[0..(char_num - 1)]
end

def first_word(str)
  str.split(" ")[0]
end

def titleize(str)
  small_words = ["and", "over", "the"]
  capped =  str.capitalize
  capped.split(" ").map do |word|
    if small_words.include?(word)
      word
    else
      word.capitalize
    end
  end.join(" ")
end
