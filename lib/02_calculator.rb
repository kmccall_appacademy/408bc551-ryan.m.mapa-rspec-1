def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  arr.reduce(0) {|acc, num| acc + num}
end

def multiply(arr)
  arr.reduce(1) {|acc, num| acc * num}
end

def power(num1, num2)
  num1 ** num2
end

def factorial(num)
  return 1 if num == 0
  (1..num).reduce(:*)
end
