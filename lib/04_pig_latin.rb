require 'byebug'

# Rule 1: If a word begins with a vowel sound, add an "ay" sound to
# the end of the word.
#
# Rule 2: If a word begins with a consonant sound, move it to the end
# of the word, and then add an "ay" sound to the end of the word.
#
# (There are a few more rules for edge cases, and there are regional
# variants too, but that should be enough to understand the tests.)
#

def translate(str)
  words = str.split(" ")
  vowels = "aeiou"

  words.map do |word|
    if vowels.include?(word[0])
      word + "ay"
    elsif !vowels.include?(word[0])
      word_arr = word.chars
      word_arr.rotate! until vowels.include?(word_arr[0])

      word_arr.rotate! if word_arr[0] == "u" && word_arr.last == "q"

      word_arr.join + "ay"
    end
  end.join(" ")

end
